(use (srfi 1 4) test sentence-split)

(define test-string-1
  "In the summers there is one visitor, however, to that valley, of
which the Yeehats do not know. It is a great, gloriously coated
wolf, like, and yet unlike, all other wolves. He crosses alone from
the smiling timber land and comes down into an open space among the
trees. Here a yellow stream flows from rotted moose-hide sacks and
sinks into the ground, with long grasses growing through it and
vegetable mould overrunning it and hiding its yellow from the sun; and
here he muses for a time, howling once, long and mournfully, ere he
departs. But he is not always alone. When the long winter nights come on
and the wolves follow their meat into the lower valleys, he may be
seen running at the head of the pack, through the pale moonlight or
glimmering borealis, leaping gigantic above his fellows, his great
throat a-bellow as he sings a song of the younger world, which is
the song of the pack.")

(define test-string-2
  "The Army Physical Fitness Test (APFT) is designed to test the muscular
  strength, endurance, and cardiovascular respiratory fitness of soldiers
  in the Army. Soldiers are scored based on their performance in three 
  events consisting of the push-up, sit-up, and a two-mile run, ranging 
  from 0 to 100 points in each event. A minimum score of 60 in each event
  is required to pass the test. The soldier\'s overall score is the sum of
  the points from the three events. If a soldier passes all three events,
  the total may range from 180 to 300. Active component and reserve 
  component soldiers on active duty are required to take a \"record\"
  (meaning for official records) APFT at least twice each calendar year.
  Reservists not on active duty must take a \"record\" test once per calendar
  year. [1] FM 7-22 covers the administration of the APFT, as well as ways to
  conduct individual, squad and unit level physical training sessions.")

(define test-string-3
  "Pr. Chamberlain was also a col. in the U.S.A. army. He had three tenets in 
  a battle: 1. Never waste time 2. Surprise plays a much greater role in 
  tactics than strategy 3. Cut off the enemy from his line of retreat. He is
  most well known for his gallantry at the Battle of Gettysburg, for which he
  was awarded the Medal of Honor. Chamberlain was commissioned a lt. col. in 
  the 20th Maine Volunteer Infantry Regiment in 1862 and fought at the Battle
  of Fredericksburg. He became commander of the regiment in June 1863. On July
  2nd, during the Battle of Gettysburg, Chamberlain's regiment occupied the
  extreme left of the Union lines at Little Round Top. Chamberlain's men 
  withstood repeated assaults from the 15th Regiment Alabama Infantry and
  finally drove the Confederates away with a bayonet charge. Chamberlain was
  severely wounded while commanding a brigade during the Second Battle of 
  Petersburg in June 1864, and was given what was intended to be a death bed
  promotion to brigadier general. In April 1865, he fought at the Battle of
  Five Forks and was given the honor of commanding the Union troops at the
  surrender ceremony for the infantry of Robert E. Lee's Army at Appomattox
  Court House, Virginia. After the war, he entered politics as a Republican
  and served four one-year terms of office as the 32nd Governor of Maine. He
  served on the faculty, and as president, of his alma mater, Bowdoin College.
  He died in 1914 at age 85 due to complications from the wound that he 
  received at Petersburg.")


(define (show-sentences text)
  (for-each
    (lambda (x)
      (print "<" x ">"))
    (get-sentences text)))

(test-begin "sentence-split")
    (test "test1" 6  (length (get-sentences test-string-1)))
    (test "test2" 8  (length (get-sentences test-string-2)))
    (test "test3" 12 (length (get-sentences test-string-3)))
(test-end "sentence-split")
(test-exit)
