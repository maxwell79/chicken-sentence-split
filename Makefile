default: build

build:
	chicken-install -n 

install:
	chicken-install

install-test:
	chicken-install -test

uninstall:
	chicken-uninstall sentence-split

test:
	cd tests && csi -s run.scm

clean:
	rm -f *.import.scm *.log *.types *.c *.o *.so ./examples/example
