# chicken-sentence-split


CHICKEN Scheme port of [Lingua::EN::Sentence](https://github.com/kimryan/Lingua-EN-Sentence) Perl module that extracts sentences from a body of text. This project was originated by Shlomo Yona. The Perl module is currently maintained by Kim Ryan

* Version:     0.1 (2018-01-25)
* API Docs:    https://wiki.call-cc.org/eggref/4/sentence-split
* License:     GNU GPL v1.0, Artistic License 1.0
* Maintainer:  David Ireland (djireland79 at gmail dot com)
* Installation: chicken-install sentence-split

