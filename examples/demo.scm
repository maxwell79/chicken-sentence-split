(use sentence-split)

(define (test-from-file)
  (let ((cnt 1))
    (for-each 
      (lambda (s)
        (print "Sentence [" cnt "]:"  s)
        (set! cnt (+ cnt 1)))
      (get-sentences
        (with-input-from-port
          (open-input-file "callwild.txt") 
          read-string)))))


(define (test-string) 
  (get-sentences 
    "First sentence. 12. point 12.  Some numbers 12.46, -.123,3:.
     Some \\`quotes wrap this\\` {\"And more\"}"))


